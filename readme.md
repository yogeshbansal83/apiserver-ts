## Repository url and clone command

git clone https://yogeshbansal83@bitbucket.org/yogeshbansal83/apiserver-ts.git

## Install the libraries

npm install

## Start the Server

npm start

## GrpahQL playground URL

http://localhost:8445/graphql

## Run Unit Test

### Go to src/server.ts file and set the mocks : true ( it will mock the api response for testing purpose)

npm run test -- -u

## Generate Coverage Report

npm run coverage -- -u

## Get Properties or update favorite counter

query PROPERTIES{ properties(city:"Katy"){ listingId listPrice property { area bedrooms } address{full} disclaimer favoriteCount createdAt } }

mutation SAVE_FAVORITES{ saveFavorites( newFavorite:{ listingId:"79978809" favoriteCount:1 } ){ listingId favoriteCount createdAt } }

## Register User and Authentication

query ME{ me{ \_id name email password } }

query ALL_USERS{ allUsers{ name email password } }

query USER_BY_ID{ user(id:"6028d333d2af0d1585de7c29"){ name email password } }

mutation USER_CREATE{ registerUser( name:"Yogesh Bansal" email:"testuser100@yopmail.com" password:"test123" ){ token } }

mutation Login{ login( email:"testuser100@yopmail.com" password:"test123" ){ token user { \_id name } } }

## Add the Authorization Header before fetching the property listing

{ "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDI5ZmExZDAwN2YyNjMyOTgwNTlmMmMiLCJlbWFpbCI6InRlc3R1c2VyMTAwQHlvcG1haWwuY29tIiwiaWF0IjoxNjEzNDcxMjk2LCJleHAiOjE2MTM1NTc2OTZ9.rF9Wbn_MxV7fOG15bqVwxfePR6vjUgPJ9ncRQXSlC9Q" }

## Project Folder Structure

apiserver-ts -> root folder  
 --coverage -> auto generated unit test coverage report  
 --node_modules -> libraries and packages required for success execute of code  
 --src -> source code root  
 --apis -> api entry point like health check etc  
 --configuration -> folder contains loggin mechanism and other configuration required in the project  
 --graphql -> contains the graphql resolvers and type defs (main folder)  
 --routes -> it can contains all the api endpoint routes  
 --tests -> it contains the test cases ( unit and integration test files)  
 --server.ts -> enter point of the server  
 --.gitignore -> file to ignore folder and files to checking into repo  
 --jest.config,js -> jest configuration to run the test caes  
 --package.json -> it contains all command and packages

## Steps to start server and test graphql endpoint

    1. git clone https://yogeshbansal83@bitbucket.org/yogeshbansal83/apiserver-ts.git
    2. npm install
    3. npm start
    4. Open the browser and go to the *[GraphQl Playground](http://localhost:8445/graphql)*
    5. First use the mutation to login
       mutation Login{ login( email:"testuser100@yopmail.com" password:"test123" ){ token user { \_id name } } }
    6. Get the token and send it in header while making query for propety listing
    5. write the query and mutations (sample query and mutation given above)

## Design

    Objective of the assignment was to get the property listing from the Simplyrets. Few of the main features of the applications

    1. Get the Property List from Simplyrets by city name
    2. Initialize the favorite counter to 0
    3. Increment the favorite counter by 1
    4. User Authentication before accessing the property list.

    When user request for the property listing, first we check it in the database, it property listing with the city name exist
    in the database, then return the properties to client. but if the property does not exist in the databasem, then request the
    Simplyrets api to get the propperty list. After getting the properties from Simplyrets, return the propety listing to client
    as well as update he database also. Before inserting the property to database or sending to client, added a property 'favorite counter'
    to the property listing and save it to database also. When client send a request to update favorite counter it will find it in database based
    on the listingId and increment the favorite counter value by 1.
