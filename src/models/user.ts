import { model, Schema } from 'mongoose';

const UserSchema = new Schema(
    {
        email: {
            type: String,
            required: true,
            unique: true
        },
        name: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        }
    },
    { timestamps: true }
);

UserSchema.index({ email: 1 }, { unique: true });
const User = model('user', UserSchema);

export default User;
