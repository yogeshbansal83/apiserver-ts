import { model, Schema } from 'mongoose';

const PropertySchema = new Schema(
    {
        listingId: {
            type: String,
            required: true,
            unique: true
        },
        favoriteCount: {
            type: Number,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        listPrice: {
            type: String,
            required: true
        },
        property: {
            area: {
                type: String
            },
            bedrooms: {
                type: String
            }
        },
        address: {}
    },
    { timestamps: true }
);

PropertySchema.index({ listingId: 1 }, { unique: true });
const Property = model('Property', PropertySchema);

export default Property;
