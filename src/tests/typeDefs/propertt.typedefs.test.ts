import { gql } from 'apollo-server-express';
import propertyDefs from '../../graphql/typeDefs/propertyDefs';

test('check if type Property has correct fields', () => {
    expect(propertyDefs).toBe(gql`
        extend type Query {
            properties(city: String!): [Property!]
        }

        extend type Mutation {
            saveFavorites(newFavorite: NewFavorite): Property!
        }

        input NewFavorite {
            listingId: String!
            favoriteCount: Int!
        }

        type Property {
            listingId: String!
            listPrice: String
            property: propertyDetail
            address: Address
            disclaimer: String
            favoriteCount: Int
            createdAt: String
            updatedAt: String
        }

        type Address {
            crossStreet: String
            state: String
            country: String
            postalCode: String
            streetName: String
            streetNumberText: Int
            city: String
            streetNumber: Int
            full: String
            unit: String
        }

        type propertyDetail {
            area: String
            bedrooms: String
        }
    `);
});
