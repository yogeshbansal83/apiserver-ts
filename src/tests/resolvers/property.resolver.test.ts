import axios from 'axios';
import mockingoose from 'mockingoose';
import { errorMessages } from '../../configuration/constants';
import propertyResolver from '../../graphql/resolvers/propertyResolver';
import Property from '../../models/property';
jest.mock('axios');

describe('Property Listing Tests', () => {
  it('Update the Favorite Count Test', async () => {
    mockingoose(Property).toReturn(
      {
        _id: '6029b92490244320c0d24dab',
        listingId: 'Hello World',
        favoriteCount: -72,
      },
      'findOne'
    );
    mockingoose(Property).toReturn(
      {
        _id: '6029b92490244320c0d24dab',
        listingId: 'Hello World',
        favoriteCount: 5,
      },
      'findOneAndUpdate'
    );
    const result = await propertyResolver.Mutation.saveFavorites(
      null,
      {
        newFavorite: { listingId: '79978809', favoriteCount: 1 },
      },
      {
        user: {
          _id: '6028d333d2af0d1585de7c29',
          email: 'testuser100@yopmail.com',
          iat: 1613359828,
          exp: 1613446228,
        },
      }
    );
    expect(JSON.parse(JSON.stringify(result))).toMatchObject({
      _id: '6029b92490244320c0d24dab',
      listingId: 'Hello World',
      favoriteCount: 5,
    });
  });
  it('Failed To Update Favorite Count', async () => {
    mockingoose(Property).toReturn(null, 'findOne');
    const result = await propertyResolver.Mutation.saveFavorites(
      null,
      {
        newFavorite: { listingId: '79978809', favoriteCount: 1 },
      },
      {
        user: {
          _id: '6028d333d2af0d1585de7c29',
          email: 'testuser100@yopmail.com',
          iat: 1613359828,
          exp: 1613446228,
        },
      }
    );
    expect(result).toEqual(new Error(errorMessages.PROPERTY_NOT_FOUND));
  });

  it('Get Existing Property Listings Test', async () => {
    const _doc = [
      {
        _id: '6029be2e66578124b38ce8e7',
        listingId: 'Hello World',
        listPrice: 'Hello World',
        property: { area: 'Hello World', bedrooms: 'Hello World' },
        address: { full: 'Hello World' },
        favoriteCount: 85,
      },
      {
        _id: '6029be2e66578124b38ce8e8',
        listingId: 'Hello World',
        listPrice: 'Hello World',
        property: { area: 'Hello World', bedrooms: 'Hello World' },
        address: { full: 'Hello World' },
        favoriteCount: -50,
      },
    ];
    mockingoose(Property).toReturn(_doc, 'find');
    const result = await propertyResolver.Query.properties(
      null,
      {
        city: 'katy',
      },
      {
        user: {
          _id: '6028d333d2af0d1585de7c29',
          email: 'testuser100@yopmail.com',
          iat: 1613359828,
          exp: 1613446228,
        },
      }
    );
    expect(JSON.parse(JSON.stringify(result))).toMatchObject(_doc);
  });

  it('Get Properties From Simplrets Test', async () => {
    const _doc = {
      data: [
        {
          _id: '6029be2e66578124b38ce8e7',
          listingId: 'Hello World',
          listPrice: 'Hello World',
          property: { area: 'Hello World', bedrooms: 'Hello World' },
          address: { full: 'Hello World' },
          city: undefined,
          favoriteCount: 0,
        },
        {
          _id: '6029be2e66578124b38ce8e8',
          listingId: 'Hello World',
          listPrice: 'Hello World',
          property: { area: 'Hello World', bedrooms: 'Hello World' },
          address: { full: 'Hello World' },
          city: undefined,
          favoriteCount: 0,
        },
      ],
    };
    (axios.get as jest.Mock).mockImplementationOnce(() => Promise.resolve(_doc));
    mockingoose(Property).toReturn([], 'find');
    const result = await propertyResolver.Query.properties(
      null,
      {
        city: 'katy',
      },
      {
        user: {
          _id: '6028d333d2af0d1585de7c29',
          email: 'testuser100@yopmail.com',
          iat: 1613359828,
          exp: 1613446228,
        },
      }
    );

    expect(result).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          listingId: 'Hello World',
        }),
      ])
    );
  });
  it('Failed to get properties from simplrets', async () => {
    const _doc = {
      data: [
        {
          _id: '6029be2e66578124b38ce8e7',
          listingId: 'Hello World',
          listPrice: 'Hello World',
          property: { area: 'Hello World', bedrooms: 'Hello World' },
          address: { full: 'Hello World' },
          city: undefined,
          favoriteCount: 0,
        },
        {
          _id: '6029be2e66578124b38ce8e8',
          listingId: 'Hello World',
          listPrice: 'Hello World',
          property: { area: 'Hello World', bedrooms: 'Hello World' },
          address: { full: 'Hello World' },
          city: undefined,
          favoriteCount: 0,
        },
      ],
    };
    (axios.get as jest.Mock).mockImplementationOnce(() => Promise.reject('Api Failed'));
    mockingoose(Property).toReturn([], 'find');
    const result = await propertyResolver.Query.properties(
      null,
      {
        city: 'katy',
      },
      {
        user: {
          _id: '6028d333d2af0d1585de7c29',
          email: 'testuser100@yopmail.com',
          iat: 1613359828,
          exp: 1613446228,
        },
      }
    );
    expect(result).toEqual(new Error(errorMessages.FAILED_TO_GET_PROPERTIES));
  });

  it('Failed to get properties If User is not logged in', async () => {
    const result = await propertyResolver.Query.properties(
      null,
      {
        city: 'katy',
      },
      {
        user: null,
      }
    );
    expect(result).toEqual(new Error(errorMessages.USER_NOT_AUTHENTICATED));
  });
  it('Failed to update favorite count If User is not logged in', async () => {
    const result = await propertyResolver.Mutation.saveFavorites(
      null,
      {
        newFavorite: { listingId: '79978809', favoriteCount: 1 },
      },
      {
        user: null,
      }
    );
    expect(result).toEqual(new Error(errorMessages.USER_NOT_AUTHENTICATED));
  });
});
