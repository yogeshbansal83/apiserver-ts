import { errorMessages } from '../../configuration/constants';
import { queryGraphQl } from '../../configuration/util';
let token: string;
beforeAll(async () => {
  const query = `
    mutation {
      login(email: "testuser100@yopmail.com", password: "test123") {
        token
        user {
          _id
          name
        }
      }
    }
  `;
  try {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const loginResult = await queryGraphQl(query, headers);
    const login = loginResult.data;
    token = login.data.login.token;
  } catch (error) {
    console.log('error', error);
  }
});

describe('Getting Property Listings', () => {
  it('Get properties by city name', async () => {
    const query = `
      query {
        properties(city: "Houston") {
          listingId
          listPrice
          property {
            area
            bedrooms
          }
          address {
            full
          }
          disclaimer
          favoriteCount
          createdAt
        }
      }
    `;

    const headers = {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const response = await queryGraphQl(query, headers);
    expect(response.data).toMatchSnapshot();
  });

  it('Updated the favorite count by 1', async () => {
    const query = `
      mutation {
        saveFavorites(newFavorite: { listingId: "79978809", favoriteCount: 1 }) {
          listingId
          favoriteCount
          createdAt
        }
      }
    `;

    const headers = {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const response = await queryGraphQl(query, headers);
    expect(response.status).toEqual(200);
  });
  it('Failed to get property list, User not logged in', async () => {
    const query = `
      query {
        properties(city: "Houston") {
          listingId
          listPrice
          property {
            area
            bedrooms
          }
          address {
            full
          }
          disclaimer
          favoriteCount
          createdAt
        }
      }
    `;
    const headers = {
      Authorization: `Bearer`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const response = await queryGraphQl(query, headers);
    expect(response.data.errors[0].message).toEqual(errorMessages.USER_NOT_AUTHENTICATED);
    expect(response.status).toEqual(200);
  });
  it('Failed to update property favorite counter by 1, if User not logged in', async () => {
    const query = `
      mutation {
        saveFavorites(newFavorite: { listingId: "79978809", favoriteCount: 1 }) {
          listingId
          favoriteCount
          createdAt
        }
      }
    `;
    const headers = {
      Authorization: `Bearer`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const response = await queryGraphQl(query, headers);
    expect(response.data.errors[0].message).toEqual(errorMessages.USER_NOT_AUTHENTICATED);
    expect(response.status).toEqual(200);
  });
});
