import baseDefs from './baseDefs';
import propertyDefs from './propertyDefs';
import userDefs from './userDefs';
export default [baseDefs, propertyDefs, userDefs];
