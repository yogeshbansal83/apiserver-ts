import { gql } from 'apollo-server-express';
export default gql`
    type User {
        _id: String!
        name: String
        email: String!
        password: String!
    }
    type AuthResponse {
        token: String!
        user: User!
    }
    extend type Query {
        user(id: String!): User
        allUsers: [User!]!
        me: User
    }
    extend type Mutation {
        registerUser(name: String, email: String!, password: String!): AuthResponse!
        login(email: String!, password: String!): AuthResponse!
    }
`;
