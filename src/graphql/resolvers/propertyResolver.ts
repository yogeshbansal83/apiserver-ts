import axios from 'axios';
import { errorMessages, URLS } from '../../configuration/constants';
import { Property } from '../../models';
export default {
  Query: {
    properties: async (_: any, { city }: any, { user }: any) => {
      try {
        if (!user) {
          return new Error(errorMessages.USER_NOT_AUTHENTICATED);
        }
        const listingResult = await Property.find({ city: city });
        if (Array.isArray(listingResult) && listingResult.length > 0) {
          return listingResult;
        } else {
          var auth = Buffer.from('simplyrets' + ':' + 'simplyrets').toString('base64');
          const response = await axios.get(`${URLS.SIMPLYRETS_URL}?q=${city}`, {
            headers: {
              Authorization: `Basic ${auth}`,
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          });
          const listingList = response.data.map((property: any) => {
            return {
              listingId: property.listingId,
              favoriteCount: 0,
              city: property.address.city,
              listPrice: property.listPrice,
              property: {
                area: property.property.area,
                bedrooms: property.property.bedrooms,
              },
              address: property.address,
            };
          });
          await Property.insertMany(listingList, { ordered: false });
          return listingList;
        }
      } catch (error) {
        return new Error(errorMessages.FAILED_TO_GET_PROPERTIES);
      }
    },
  },
  Mutation: {
    saveFavorites: async (_: any, { newFavorite }: any, { user }: any) => {
      let result;
      if (!user) {
        return new Error(errorMessages.USER_NOT_AUTHENTICATED);
      }
      const listing = await Property.findOne({ listingId: newFavorite.listingId });
      if (listing) {
        result = await Property.findOneAndUpdate(
          { listingId: newFavorite.listingId },
          { $inc: { favoriteCount: 1 } },
          {
            upsert: false,
            new: true,
          }
        );
        return result;
      } else {
        return new Error(errorMessages.PROPERTY_NOT_FOUND);
      }
    },
  },
};
