import propertyResolver from './propertyResolver';
import userResolvers from './userResolvers';
export default [propertyResolver, userResolvers];
