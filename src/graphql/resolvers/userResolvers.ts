import * as bcrypt from 'bcrypt';
import jsonwebtoken from 'jsonwebtoken';
import { constants, errorMessages } from '../../configuration/constants';
import * as models from '../../models';
export default {
  Query: {
    me: async (_: any, args: any, { user }: any) => {
      if (!user) throw new Error(errorMessages.USER_NOT_AUTHENTICATED);
      return await models.User.findById(user._id);
    },
    user: async (_: any, { id }: any, { user }: any) => {
      try {
        if (!user) throw new Error(errorMessages.USER_NOT_AUTHENTICATED);
        return models.User.findById(id);
      } catch (error) {
        throw new Error(error.message);
      }
    },
    allUsers: async (_: any, args: any, { user }: any) => {
      try {
        if (!user) throw new Error(errorMessages.USER_NOT_AUTHENTICATED);
        return models.User.find({});
      } catch (error) {
        throw new Error(error.message);
      }
    },
  },
  Mutation: {
    registerUser: async (_: any, { name, email, password }: any) => {
      try {
        const user = await models.User.create({
          name,
          email,
          password: await bcrypt.hash(password, 10),
        });
        const token = jsonwebtoken.sign({ _id: user._id, email: email }, constants.JWT_SECRET, {
          expiresIn: '1d',
        });
        return {
          token,
          _id: user._id,
          name: name,
          email: email,
          message: constants.AUTHENTICATION_SUCCESSFULL,
        };
      } catch (error) {
        throw new Error(error.message);
      }
    },
    login: async (_: any, { email, password }: any) => {
      try {
        const user: any = await models.User.findOne({ email });
        if (!user) {
          throw new Error(errorMessages.NO_USER_WITH_EMAIL);
        }
        const isValid = await bcrypt.compare(password, user.password);
        if (!isValid) {
          throw new Error(errorMessages.INCORRECT_PASSWORD);
        }
        // return jwt
        const token = jsonwebtoken.sign({ _id: user._id, email }, constants.JWT_SECRET, {
          expiresIn: '1d',
        });
        return {
          token,
          user,
        };
      } catch (error) {
        throw new Error(error.message);
      }
    },
  },
};
