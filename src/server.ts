import { ApolloServer } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import jsonwebtoken from 'jsonwebtoken';
import mongoose from 'mongoose';
import { constants, DB, errorMessages } from '../src/configuration/constants';
import logger from './configuration/logging';
import { resolvers, typeDefs } from './graphql';
const NAMESPACE = 'SERVER';

// Initialize the express appplication
const app = express();

const getUser = (token: any) => {
  try {
    if (token) {
      return jsonwebtoken.verify(token, constants.JWT_SECRET);
    }
    return null;
  } catch (error) {
    return null;
  }
};
const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => {
    const token = req.get('Authorization') || '';
    return { user: getUser(token.replace('Bearer', '').trim()) };
  },
  playground: true, // set to false in production
  mocks: false,
});

/** Logging the request */
app.use((req, res, next) => {
  logger.info(
    NAMESPACE,
    `Method-[${req.method}],URL-[${req.url}], IP-[${req.socket.remoteAddress}]`
  );

  res.on('finish', () => {
    logger.info(
      NAMESPACE,
      `Method-[${req.method}],URL-[${req.url}], IP-[${req.socket.remoteAddress}], STATUS-[${res.statusCode}]`
    );
  });
  next();
});

/** Rules of our APIs */
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin,X-Requested-With, Content-Type,Accept,Authorization'
  );

  if (req.method == 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET PATCH DELETE POST PUT');
    return res.status(200).json({});
  }
  next();
});

const startServer = async () => {
  try {
    apolloServer.applyMiddleware({ app });
    await mongoose.connect(DB.PROD_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    logger.info(NAMESPACE, constants.CONNECTED_DATABASE);

    /** Error Handling */
    app.use((req, res, next) => {
      const error = new Error(errorMessages.NOT_FOUND);
      return res.status(404).json({
        message: error.message,
      });
    });
    /** Create the server */
    const httpServer = http.createServer(app);
    httpServer.listen(8445, () => {
      logger.info(NAMESPACE, constants.SERVER_RUNNING);
    });
  } catch (error) {
    logger.error(NAMESPACE, error.message);
  }
};

startServer();
