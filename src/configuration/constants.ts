export const constants = {
  AUTHENTICATION_SUCCESSFULL: 'Authentication succesfull',
  JWT_SECRET: 'testSecret',
  CONNECTED_DATABASE: `Connected to database successfully`,
  SERVER_RUNNING: 'Server running on',
};

export const errorMessages = {
  USER_NOT_AUTHENTICATED: 'You are not authenticated',
  FAILED_TO_GET_PROPERTIES: 'Failed to get properties',
  PROPERTY_NOT_FOUND: 'Property Not Found',
  NO_USER_WITH_EMAIL: 'No user with that email',
  INCORRECT_PASSWORD: 'Incorrect password',
  NOT_FOUND: 'Not Found',
};

export const URLS = {
  SIMPLYRETS_URL: 'https://api.simplyrets.com/properties',
  HOST_URL: 'http://localhost:8445/graphql',
};

export const DB = {
  PROD_URL:
    'mongodb+srv://yogeshbansal:JRXLk0XFXblWasFE@cluster0.uhxlt.mongodb.net/sideInc?retryWrites=true&w=majority',
  DEV_URL: 'mongodb://localhost:27017/sideInc',
};
