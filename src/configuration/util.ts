import axios from 'axios';
import { URLS } from './constants';
export const queryGraphQl = async (query: any, headers: any) => {
  const response = await axios({
    method: 'POST',
    url: URLS.HOST_URL,
    data: { query },
    headers: headers,
  });
  return response;
};
